# https://machinelearningmastery.com/how-to-perform-object-detection-with-yolov3-in-keras/
# https://github.com/experiencor/keras-yolo3


# okee, my laptop cant handle the truth!
# moet maar ml op aws doen. -> sagemaker?  duurste instance: ml.p3.16xlarge: $37.016, goedkoopste: ml.t2.medium: $0.05
# https://docs.aws.amazon.com/dlami/latest/devguide/tutorial-keras.html

import glob
import matplotlib.pyplot as plot

from keras.applications.inception_v3 import InceptionV3, preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD

import keras
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D
from keras.callbacks import ModelCheckpoint

import os
import json
import numpy as np

from system import getNumberOfFiles, getNumberOfSubFolders
from history import saveHistory, loadHistory, appendHistory

def train(TrainingEpochs):
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = '2'
    with open('./settings.json', 'r') as f:
        settings = json.load(f)


    def createImageGenerator():
        return ImageDataGenerator(
            preprocessing_function=preprocess_input,
            rotation_range=30,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )

    ImageWidth, ImageHeight = 1600, 256
    BatchSize = 1
    NumberOfNeuronsFullyConnectedClassifictationLayer = 256

    TrainDirectory = settings['TRAIN_DATA_CLEAN_PATH']
    ValidateDirectory = settings['VALIDATE_DATA_CLEAN_PATH']

    NumberOfTrainingSamples = getNumberOfFiles(TrainDirectory)
    NumberOfClasses = getNumberOfSubFolders(TrainDirectory)
    NumberOfValidationSamples = getNumberOfFiles(ValidateDirectory)

    TrainImageGenerator = createImageGenerator().flow_from_directory(
        TrainDirectory,
        target_size=(ImageWidth, ImageHeight),
        batch_size=BatchSize,
        seed=42
    )
    ValidationImageGenerator = createImageGenerator().flow_from_directory(
        ValidateDirectory,
        target_size=(ImageWidth, ImageHeight),
        batch_size=BatchSize,
        seed=42
    )

    #loading the pretrained model
    # include_top=False removes the classification layer, so we can add our own
    InceptionV3BaseModel = InceptionV3(weights='imagenet', include_top=False)
    print('Inception v3 base model without classification layer loaded.')

    x = InceptionV3BaseModel.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(NumberOfNeuronsFullyConnectedClassifictationLayer,
              activation='relu')(x)
    predictions = Dense(NumberOfClasses, activation='softmax')(x)

    model = Model(inputs=InceptionV3BaseModel.input, outputs=predictions)


    # to prevent training the layers of the base model:
    for layer in InceptionV3BaseModel.layers:
        layer.trainable = False

    # print(model.summary())

    # loading previously trained weights, if they exist
    nameModelFile = 'inceptionv3-transfer-learning.model'
    modelDirectory = settings['MODEL_DIR']
    modelPath = os.path.join(modelDirectory, nameModelFile).replace("\\", "/")
    # if os.path.isfile(modelPath):
    #     model.load_weights('./checkpoints/inceptionv3-transfer-learning.model')

    # Instead of training all over everytime, load previously trained model, including its history:
    numberOfEpochsTrained = 0
    oldTrainingHistory = {}
    latestCheckpointPath = os.path.join(modelDirectory,'latestCheckpoint.hdf5').replace("\\", "/")
    latestTrainingHistoryPath = os.path.join(modelDirectory,'latestTrainingHistory.json').replace("\\", "/")

    if os.path.exists(latestCheckpointPath) and os.path.exists(latestTrainingHistoryPath):
        model = keras.models.load_model(latestCheckpointPath)
        oldTrainingHistory = loadHistory(latestTrainingHistoryPath)
        numberOfEpochsTrained = len(oldTrainingHistory['loss'])
        print('loaded previously trained model and its history')

    # Compile the model
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    print('Model compiled')

    # training the model
    checkpointModel = ModelCheckpoint(filepath=latestCheckpointPath)
    os.makedirs(os.path.dirname(latestCheckpointPath), exist_ok=True)
    callbackList = [checkpointModel]
    trainingHistory = model.fit_generator(
        TrainImageGenerator,
        epochs=TrainingEpochs+numberOfEpochsTrained,
        initial_epoch=numberOfEpochsTrained, # should be called previous epoch
        callbacks=callbackList,
        steps_per_epoch=NumberOfTrainingSamples // BatchSize,
        validation_data=ValidationImageGenerator,
        validation_steps=NumberOfValidationSamples // BatchSize,
        class_weight='auto'
    )
    os.makedirs(os.path.dirname(latestTrainingHistoryPath), exist_ok=True)
    trainingHistory = appendHistory(trainingHistory.history, oldTrainingHistory)
    saveHistory(latestTrainingHistoryPath, trainingHistory)
    model.save(modelPath) 
train(1)