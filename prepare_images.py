import os, json
import pandas as pd
from shutil import copyfile

def prepare_images():
    # When this function is run, It is assumed the images files are already stored locally.
    with open('./settings.json', 'r') as f:
        settings = json.load(f)

    sourceImageDirectory = os.path.join(settings['RAW_DATA_DIR'],settings['RAW_DATA_TRAIN_IMAGE_DIR']).replace("\\", "/")
    cleanTrainData = pd.read_csv(os.path.join(settings['TRAIN_DATA_CLEAN_DIR'],'training.csv'),index_col=0, names=['fileName', 'hasDefect']) 
    cleanTrainImagesDirectory = os.path.join(settings['TRAIN_DATA_CLEAN_DIR'],settings['RAW_DATA_TRAIN_IMAGE_DIR']).replace("\\", "/")

    for _, subDirectoryNames, _ in os.walk(cleanTrainImagesDirectory):
        # subDirectoryNames should be equal to classNames
        for subDirectory in subDirectoryNames:
            className = subDirectory
            for _, _, files in os.walk(os.path.join(cleanTrainImagesDirectory, subDirectory)):
                for fileName in files:  
                    path = os.path.join(cleanTrainImagesDirectory,subDirectory,fileName).replace("\\", "/")                
                    if not (cleanTrainData.index == fileName).any():
                        print(fileName, 'is currently not in the training set and thus will be removed')
                        os.remove(path)
                    elif str(cleanTrainData.loc[fileName, :]['hasDefect']) != str(className):
                        print(fileName, ' was found in directory', key, 'but its hasDefect value is:',cleanTrainData.loc[fileName, :]['hasDefect'],'and thus will be removed')
                        os.remove(path)
                    else:                
#                         print(fileName, 'is part of the training set and in the correct folder')
                        pass
    print('Images not in the correct folder have been removed (if any).')
  
    os.makedirs(os.path.join(cleanTrainImagesDirectory,'0'), exist_ok=True)
    os.makedirs(os.path.join(cleanTrainImagesDirectory,'1'), exist_ok=True)
    
    # check if object is already there, if not, move it there
    filesMoved=0
    for _, row in cleanTrainData.iterrows():
        fileName = row.name
        className = str(row.values[0])
        
        targetPath = os.path.join(cleanTrainImagesDirectory,className,fileName).replace("\\", "/")

        if not os.path.isfile(targetPath):
            sourcePath = os.path.join(sourceImageDirectory,fileName).replace("\\", "/")
            print('copying',sourcePath,'to', targetPath)
            copyfile(sourcePath, targetPath)
            filesMoved += 1
        
    print(filesMoved,'(additional?) images moved to their correct subfolders')
    num_training_samples = len(cleanTrainData.index)
    print('Number of training samples:',num_training_samples)
    return num_training_samples
