import numpy as np
import pandas as pd
import cv2
import os
import json
from tqdm import tqdm
import matplotlib.pyplot as plt


with open('./settings.json', 'r') as f:
    settings = json.load(f)

dataFrame_train = pd.read_csv(os.path.join(settings['RAW_DATA_DIR'],'train.csv'))
print(dataFrame_train)

temp = dataFrame_train["ImageId_ClassId"].str.split("_", n=1, expand=True)
dataFrame_train["ImageId"] = temp[0]
dataFrame_train["ClassId"] = temp[1]
dataFrame_train.drop(columns=["ImageId_ClassId"], inplace=True)
print(dataFrame_train)

faultyDataFrame = dataFrame_train.dropna(subset=['EncodedPixels'])
notFaultyDataFrame = dataFrame_train.loc[dataFrame_train.isnull()['EncodedPixels']]
print(faultyDataFrame)
print(faultyDataFrame.describe())
print(notFaultyDataFrame)
print(notFaultyDataFrame.describe())


# multi-label classification problem; some have multiple type of defects
#   suggest sigmoid rather than softmax as activation function for output layer and multi hot encoding [1 0 1]

classCount = faultyDataFrame.groupby('ClassId').count()
print(classCount)

# most appear to be of type 3, thus imbalanced training set. 5000 vs 250...
#   Possible solutions: undersampling of majority class, oversampling of minority class, undersampling+reshuffle

# Regarding faulty vs non-faulty
# There are 7095 faulty images and 43177 non-faulty images. Also unbalanced training set.

# For now I guess we'll use undersampling, it will also speed up training




# I want to visualize one image with the encoded pixel mask, just as an example.


train = pd.read_csv('data/input/train.csv')

# from https://www.kaggle.com/paulorzp/rle-functions-run-lenght-encode-decode
def mask2rle(img):
    '''
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    pixels = img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)

# from https://www.kaggle.com/paulorzp/rle-functions-run-lenght-encode-decode
def rle2mask(mask_rle, shape=(1600, 256)):
    '''
    mask_rle: run-length as string formated (start length)
    shape: (width,height) of array to return 
    Returns numpy array, 1 - mask, 0 - background

    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int)
                       for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape).T


# Test RLE functions
assert mask2rle(rle2mask(train['EncodedPixels'].iloc[0])
                ) == train['EncodedPixels'].iloc[0]
assert mask2rle(rle2mask('1 1')) == '1 1'

def getBorder(mask):
    rows =  mask.shape[0]
    columns = mask.shape[1]
    border = np.zeros((rows, columns))
    for i in range(2,rows-3):
        for j in range(2,columns-3):
            if mask[i,j] == 1:
                for u in range(i-2,i+2):
                    for v in range(j-2,j+2):
                        if mask[u,v] == 0:
                            border[i,j] = 1
    return border


def widenBorder(mask):
    rows = mask.shape[0]
    columns = mask.shape[1]
    border = np.zeros((rows, columns))
    for i in range(1, rows-2):
        for j in range(1, columns-2):
            if mask[i, j] == 1:
                for u in range(i-1, i+1):
                    for v in range(j-1, j+1):
                        border[u, v] = 1
    return border

im_dir = './data/input/train_images'
# Get all image files
all_items = os.listdir(im_dir)
image_files = [file for file in all_items if os.path.isfile(
    os.path.join(im_dir, file))]

imageid_classid = []
encodedpixels = []
for image_file in tqdm(image_files):
    im_name = image_file.split('.')[0]
    im_path = os.path.join(im_dir, image_file)

    # Get image data
    image = cv2.imread(im_path, 0)
    image = image.reshape(256, 1600, 1)
    image = image / 255.

    image2 = image

    print('image name:', im_name)
    rowWithEncodedPixels = faultyDataFrame['ImageId'] == im_name + '.jpg'
    encodedPixels = faultyDataFrame[rowWithEncodedPixels].EncodedPixels
    encodedPixels = encodedPixels.values[0]
    print(encodedPixels)
    mask = rle2mask(encodedPixels)

    mask = getBorder(mask)
    # mask = widenBorder(mask)
    mask = mask.reshape(256, 1600, 1)

    # mask = cv2.cvtColor(mask.astype(np.float32), cv2.COLOR_GRAY2BGR)
    maskInverted = 1-mask

    # image = cv2.cvtColor(image.astype(np.float32), cv2.COLOR_GRAY2BGR)
    image2 = image

    image2 = np.multiply(image2, maskInverted) 


    combinedImage = np.vstack((image,image2))

    cv2.imshow('Image from training set, image belows shows the identified defects', combinedImage)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    break
