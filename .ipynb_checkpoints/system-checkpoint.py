import shutil
import os
import boto3


def getNumberOfFiles(path):
    if not os.path.exists(path):
        return 0
    return sum([len(files) for r, d, files in os.walk(path)])


def getNumberOfSubFolders(path):
    if not os.path.exists(path):
        return 0
    for r, d, files in os.walk(path):
        return(len(d))
        break

def downloadAllFilesFromS3ToLocal(bucketName,prefix,targetDirectory):
    s3 = boto3.client('s3')
    s3_resource = boto3.resource('s3')
    bucket = s3_resource.Bucket(bucketName) 
    
    if not os.path.exists(targetDirectory):
        os.makedirs(targetDirectory)
    
    response = list(get_all_s3_objects(s3,Bucket=bucketName, Prefix=prefix))
    
    firstImageCopied = False
    for obj in response: 
        key = obj['Key']
        fileName = os.path.split(key)[-1]
        targetKey = os.path.join(targetDirectory,fileName).replace("\\", "/")
        bucket.download_file(key,targetKey)
        
        if not firstImageCopied:
            print("first image details:")
            print("key:",key)
            print("fileName:",fileName)
            print("targetKey:",targetKey)
            firstImageCopied = True
           
            
def get_all_s3_objects(s3, **base_kwargs):
    continuation_token = None
    while True:
        list_kwargs = dict(MaxKeys=1000, **base_kwargs)
        if continuation_token:
            list_kwargs['ContinuationToken'] = continuation_token
        response = s3.list_objects_v2(**list_kwargs)
        yield from response.get('Contents', [])
        if not response.get('IsTruncated'):  # At the end of the list?
            break
        continuation_token = response.get('NextContinuationToken')