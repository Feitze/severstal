import pandas as pd
BatchSize = 2
PredictImageGenerator = createImageGenerator().flow_from_directory(
    PredictDirectory,
    target_size=(ImageWidth, ImageHeight),
    batch_size=BatchSize,
    class_mode=None,
    seed=42
)
nr_samples = len(PredictImageGenerator.filenames)
print('nr_samples:', nr_samples)
predict = model.predict_generator(
    PredictImageGenerator, steps=nr_samples/BatchSize)
print('predict:', predict)
predicted_class_indices = np.argmax(predict, axis=1)
labels = (TrainImageGenerator.class_indices)
print('labels:', labels)
labels = dict((v, k) for k, v in labels.items())
print('labels:', labels)
predictions = [labels[k] for k in predicted_class_indices]

filenames = PredictImageGenerator.filenames
results = pd.DataFrame({"Filename": filenames,
                        "Predictions": predictions})
print(results)
