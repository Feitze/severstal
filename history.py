import codecs
import json
import numpy as np
import os


def saveHistory(path, history):
    with codecs.open(path, 'w', encoding='utf-8') as f:
        json.dump(history, f, separators=(',', ':'), sort_keys=True, indent=4)

def loadHistory(path):
    with codecs.open(path, 'r', encoding='utf-8') as f:
        n = json.loads(f.read())
    return n

def appendHistory(newHistory, oldHistory):
    if oldHistory == {}:
        return newHistory
    else:
        combinedHistory = oldHistory
        for key, value in newHistory.items():
            combinedHistory[key] = combinedHistory[key] + value
        return combinedHistory
