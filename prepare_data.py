import os, boto3
# data processing
import json
import pandas as pd
from pandas.api.types import CategoricalDtype

def prepare_data():    
    with open('./settings.json', 'r') as f:
        settings = json.load(f)

    # As we've seen during data analysis, there is an unbalanced training set, especially between the defect classes.
    # 5902 non-faulty vs 6666 faulty images and 5000 type3 vs 250 type2

    # So here we select the training sample we want to use, we will undersample. 
    # If called twice, it should shuffle and create a new set, but with the same balance.

    dataFrame_train = pd.read_csv(os.path.join(settings['RAW_DATA_DIR'],'train.csv'))

    # first split ImageId from the ClassId for convenience
    temp = dataFrame_train["ImageId_ClassId"].str.split("_", n=1, expand=True)
    dataFrame_train["ImageId"] = temp[0]
    dataFrame_train["ClassId"] = temp[1]
    dataFrame_train.drop(columns=["ImageId_ClassId"], inplace=True)

    # we have to group by imageId, we dont want to use the same image twice basically.
    dataFrame_train = dataFrame_train.groupby('ImageId').agg(list)
    dataFrame_train[['Class1', 'Class2', 'Class3', 'Class4']] = pd.DataFrame(dataFrame_train.EncodedPixels.values.tolist(), index=dataFrame_train.index)
    dataFrame_train = dataFrame_train.drop(columns=["ClassId",'EncodedPixels'])


    dataFrame_train['HasDefect'] = dataFrame_train[['Class1', 'Class2', 'Class3', 'Class4']].notnull().any(axis=1)
    dataFrame_train = dataFrame_train.drop(columns=['Class1', 'Class2', 'Class3', 'Class4'])

    # select the faulty set and the non-faulty set.
    faultyDataFrame = dataFrame_train[dataFrame_train['HasDefect'] == True]
    notFaultyDataFrame = dataFrame_train.drop(faultyDataFrame.index)

    # determine the number of samples per class, as we will undersample, we take the smallest set
    numberOfFaultySamples = faultyDataFrame.shape[0]
    numberOfNotFaultySamples = notFaultyDataFrame.shape[0]
    numberOfSamples = min(numberOfFaultySamples, numberOfNotFaultySamples)

    faultySamples = faultyDataFrame.sample(n=numberOfSamples)
    notFaultySamples = notFaultyDataFrame.sample(n=numberOfSamples)

    # combining both classes and even though it probably doesn't matter, in random order.
    dataFrame_train = pd.concat([faultySamples, notFaultySamples]).sample(frac=1)

    # next we need to change the data, specifically for training a model that only determines whether or not there is a fault
    dataFrame_train['HasDefect'] = dataFrame_train['HasDefect'].astype(CategoricalDtype(categories=dataFrame_train['HasDefect'].unique()))
    dataFrame_train['HasDefect'] = dataFrame_train['HasDefect'].cat.codes.astype('category')
    
    # writing the output file as csv   
    trainDirectory = settings['TRAIN_DATA_CLEAN_DIR']
    filePath = os.path.join(trainDirectory, 'training.csv').replace("\\", "/")    
    dataFrame_train.to_csv(filePath, header=False)
    
    # uploading the output file to s3
    s3 = boto3.resource('s3')
    bucket_name = settings['S3_BUCKET']   
    boto3.Session().resource('s3').Bucket(bucket_name).Object(filePath).upload_file(filePath)
